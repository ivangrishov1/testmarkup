let links = document.querySelectorAll('.header__bottom-nav ul li a');

links.forEach(item => {
    item.addEventListener('click', function () {
        links.forEach(elem => {
            elem.classList.remove('link--active');
        })
        this.classList.add('link--active');
    })
})
